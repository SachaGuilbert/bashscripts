#!/bin/bash
echo "Select option:"
echo "0) Make new dir for download"
echo "1) Download into preexisting dir"
read dirornot
if [[ $dirornot = 0 ]]
then
    echo "Enter /Music/newdirname :"
    read newdirname
    mkdir $newdirname
    echo "Paste link:"
    read pastedlink
    youtube-dl  -o "~/Music/$newdirname/%(title)s.%(ext)s" -x --audio-format mp3 $pastedlink
fi

if [[ $dirornot = 1 ]]
then
    echo "Enter path to directory where songs should be downloaded in form of /Music/dirname :"
    read dirname
    echo "Paste link:"
    read pastedlink
    youtube-dl  -o "~/Music/$dirname/%(title)s.%(ext)s" -x --audio-format mp3 $pastedlink
    else
    echo "Not a valid entry"
fi
#
#for converting to audio
# rename 's/.mkv/format/g' *; for a in *; do echo $a; ffmpeg -i $a $a.mp3 ; done
#https://www.youtube.com/watch?v=KC88uWd-ONs
