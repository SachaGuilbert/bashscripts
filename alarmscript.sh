today=`date +%s`
hours=7
minutes=40
seconds=0
# substracting 7200 seconds for timezone delay
alarmtime=$((3600*hours+60*minutes+seconds-3600*2))

while [ 1=1 ]
do
	today=`date +%s`
	time=$((today%86400))

	if [ $time = $alarmtime ]
    then
        echo Times up
        vlc test.mp3
    fi
#     echo $alarmtime
#     echo $time
    remainingtime=$(($alarmtime-$time))
    echo "Remaining time:"
    echo $remainingtime
done
