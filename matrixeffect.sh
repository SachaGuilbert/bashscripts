clear
chars=abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ1234567890
# while[1=1]
export TERM=xterm-256color
for i in {1..320} ; do
    for i in {1..40} ; do
        GREEN='\033[0;32m'
        tput setaf 2; tput setab 0; echo -n -e "${chars:RANDOM%${#chars}:1}${RED}      " | perl -pe 's/\n//'
    done
    echo
done
